//
//  RouteViewController.swift
//  Compassmap
//
//  Created by SHOKI TAKEDA on 2/12/16.
//  Copyright © 2016 compassmap.com. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class RouteViewController: UIViewController, CLLocationManagerDelegate, UISearchBarDelegate, MKMapViewDelegate, NADViewDelegate {
    @IBAction func tapScreen(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: UIView!
    private var nadView: NADView!
    var adTimer:NSTimer!
    var globalZoomLevel:Double = 1/10000
    var myMapView : MKMapView!
    var lm: CLLocationManager! = nil
    var longitude: CLLocationDegrees!
    var latitude: CLLocationDegrees!
    var globalBeading: CLLocationDirection = 0
    var myPin: MKPointAnnotation!
    var center: CLLocationCoordinate2D!
    var fromPin: MKPointAnnotation!
    var toPin: MKPointAnnotation!
    var routeRenderer: MKPolylineRenderer!
    var polyroute: MKPolyline!
    var route: MKRoute!
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        myMapView = MKMapView()
        lm = CLLocationManager()
        lm.delegate = self
        lm.requestAlwaysAuthorization()
        lm.desiredAccuracy = kCLLocationAccuracyBest
        lm.distanceFilter = 1
        lm.headingFilter = 1
        lm.startUpdatingLocation()
        lm.startUpdatingHeading()
        myPin = MKPointAnnotation()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(6.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
    }
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation){
        longitude = newLocation.coordinate.longitude
        latitude = newLocation.coordinate.latitude
        myMapView.frame = self.view.bounds
        myMapView.delegate = self
        mapView.addSubview(myMapView)
        let myCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        myMapView.removeAnnotation(myPin)
        myPin.coordinate = myCoordinate
        myMapView.addAnnotation(myPin)
        center = CLLocationCoordinate2DMake(latitude, longitude)
        let mySpan: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: globalZoomLevel, longitudeDelta: globalZoomLevel)
        let myRegion: MKCoordinateRegion = MKCoordinateRegion(center: center, span: mySpan)
        self.myMapView.region = myRegion
        myMapView.camera.heading = globalBeading + 90
    }
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        globalBeading = newHeading.magneticHeading
        myMapView.camera.heading = globalBeading + 90
    }
    func onUpdateLocation() {
        lm.startUpdatingLocation()
    }
    func onUpdateHeading() {
        lm.startUpdatingHeading()
    }
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        NSLog("Error")
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchBar.text)
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        let geocoder = CLGeocoder()
        if (self.fromPin != nil) {
            self.myMapView.self.removeAnnotation(self.fromPin)
        }
        if (self.toPin != nil) {
            self.myMapView.removeAnnotation(self.toPin)
        }
        self.myMapView.removeOverlays(myMapView.overlays)
        geocoder.geocodeAddressString(searchBar.text!) { (placemarks, error) -> Void in
            if let placemark = placemarks?[0] {
                self.myMapView.frame = self.view.bounds
                self.myMapView.delegate = self
                self.mapView.addSubview(self.myMapView)
                let requestCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(placemark.location!.coordinate.latitude, placemark.location!.coordinate.longitude)
                let fromCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.latitude, self.longitude)
                let center: CLLocationCoordinate2D = CLLocationCoordinate2DMake((placemark.location!.coordinate.latitude + self.latitude)/2, (placemark.location!.coordinate.longitude + self.longitude)/2)
                self.myMapView.setCenterCoordinate(center, animated: true)
                let mySpan: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
                let myRegion: MKCoordinateRegion = MKCoordinateRegion(center: center, span: mySpan)
                self.myMapView.region = myRegion
                let fromPlace: MKPlacemark = MKPlacemark(coordinate: fromCoordinate, addressDictionary: nil)
                let toPlace: MKPlacemark = MKPlacemark(coordinate: requestCoordinate, addressDictionary: nil)
                let fromItem: MKMapItem = MKMapItem(placemark: fromPlace)
                let toItem: MKMapItem = MKMapItem(placemark: toPlace)
                let myRequest: MKDirectionsRequest = MKDirectionsRequest()
                myRequest.source = fromItem
                myRequest.destination = toItem
                myRequest.requestsAlternateRoutes = true
                myRequest.transportType = MKDirectionsTransportType.Automobile
                let myDirections: MKDirections = MKDirections(request: myRequest)
                myDirections.calculateDirectionsWithCompletionHandler { (response, error) -> Void in
                    if error != nil || response!.routes.isEmpty {
                        return
                    }
                    self.route = response!.routes[0] as MKRoute
                    self.myMapView.addOverlay(self.route.polyline)
                }
                self.fromPin = MKPointAnnotation()
                self.toPin = MKPointAnnotation()
                self.fromPin.coordinate = fromCoordinate
                self.toPin.coordinate = requestCoordinate
                self.myMapView.self.addAnnotation(self.fromPin)
                self.myMapView.addAnnotation(self.toPin)
                self.myMapView.camera.heading = self.globalBeading+90
            } else {
                print("存在しません")
            }
        }
    }
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if (routeRenderer != nil) {
            self.routeRenderer = nil
        }
        if (route != nil) {
            self.route = nil
        }
        polyroute = overlay as! MKPolyline
        routeRenderer = MKPolylineRenderer(polyline: polyroute)
        routeRenderer.lineWidth = 3.0
        routeRenderer.strokeColor = UIColor.redColor()
        return routeRenderer
    }
    func locationManagerShouldDisplayHeadingCalibration(manager: CLLocationManager) -> Bool {
        return true
    }
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}