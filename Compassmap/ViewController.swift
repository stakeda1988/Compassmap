//
//  ViewController.swift
//  Compassmap
//
//  Created by SHOKI TAKEDA on 2/12/16.
//  Copyright © 2016 compassmap.com. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate, UISearchBarDelegate, MKMapViewDelegate, NADViewDelegate {
    @IBAction func tapScreen(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: UIView!
    private var nadView: NADView!
    var adTimer:NSTimer!
    var myMapView : MKMapView!
    var lm: CLLocationManager! = nil
    var longitude: CLLocationDegrees!
    var latitude: CLLocationDegrees!
    var myPin: MKPointAnnotation!
    var center: CLLocationCoordinate2D!
    var localSpan:MKCoordinateSpan!
    var myRegion: MKCoordinateRegion!
    var localRegion: MKCoordinateRegion!
    var globalBeading: CLLocationDirection = 0
    var globalZoomLevel:Double = 1/10000
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        myMapView = MKMapView()
        lm = CLLocationManager()
        lm.delegate = self
        lm.requestAlwaysAuthorization()
        lm.desiredAccuracy = kCLLocationAccuracyBest
        lm.distanceFilter = 1
        lm.headingFilter = 1
        lm.startUpdatingLocation()
        lm.startUpdatingHeading()
        myPin = MKPointAnnotation()
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        adTimer = NSTimer.scheduledTimerWithTimeInterval(6.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
    }
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation){
        longitude = newLocation.coordinate.longitude
        latitude = newLocation.coordinate.latitude
        myMapView.frame = self.view.bounds
        myMapView.delegate = self
        mapView.addSubview(myMapView)
        let myCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        center = CLLocationCoordinate2DMake(latitude, longitude)
        let mySpan: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: globalZoomLevel, longitudeDelta: globalZoomLevel)
        let myRegion: MKCoordinateRegion = MKCoordinateRegion(center: center, span: mySpan)
        localRegion = myRegion
        self.myMapView.region = myRegion
        myMapView.removeAnnotation(myPin)
        myPin.coordinate = myCoordinate
        myMapView.addAnnotation(myPin)
        myMapView.camera.heading = globalBeading + 90
    }
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        globalBeading = newHeading.magneticHeading
        myMapView.camera.heading = globalBeading + 90
    }
    func onUpdateLocation() {
        lm.startUpdatingLocation()
    }
    func onUpdateHeading() {
        lm.startUpdatingHeading()
    }
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        NSLog("Error")
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchBar.text)
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(searchBar.text!) { (placemarks, error) -> Void in
            if let placemark = placemarks?[0] {
                print(placemark.location!.coordinate.latitude, placemark.location!.coordinate.longitude)
                self.longitude = placemark.location!.coordinate.longitude
                self.latitude = placemark.location!.coordinate.latitude
                self.myMapView.frame = self.view.bounds
                self.myMapView.delegate = self
                self.mapView.addSubview(self.myMapView)
                let myCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.latitude, self.longitude)
                let myLatDist : CLLocationDistance = 100
                let myLonDist : CLLocationDistance = 100
                let myRegion: MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(myCoordinate, myLatDist, myLonDist);
                self.myMapView.setRegion(myRegion, animated: true)
            } else {
                // 検索リストに無ければ
                print("存在しません")
            }
        }
    }
    func locationManagerShouldDisplayHeadingCalibration(manager: CLLocationManager) -> Bool {
        return true
    }
    //    func getZoomLevel() -> Double {
    //        return log2(360 * (Double(self.view.frame.size.width/256) / self.myMapView.region.span.longitudeDelta))
    //    }
    //    func getZoomLevel() -> Double {
    //        let MERCATOR_RADIUS = 85445659.44705395
    //        let MAX_GOOGLE_LEVELS = 20
    //        let longitudeDelta:CLLocationDegrees = self.myMapView.region.span.longitudeDelta;
    //        let mapWidthInPixels:CGFloat = self.view.bounds.size.width;
    //        let zoomScale:Double = Double(longitudeDelta * MERCATOR_RADIUS * M_PI) / Double(180.0 * mapWidthInPixels)
    //        var zoomer:Double = Double(MAX_GOOGLE_LEVELS) - log2(zoomScale)
    //        if zoomer < 0{
    //            zoomer = 0
    //        }
    //        return zoomer
    //    }
    //
    
//    func mapView(mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
//        print("beforeRegionWillChangeAnimated: " + String(localRegion))
//                localRegion = self.myMapView.region
//        print("afterRegionWillChangeAnimated: " + String(localRegion))
//    }
//    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
//        print("beforeRegionDidChangeAnimated: " + String(localRegion))
//                localRegion = self.myMapView.region
//        print("afterRegionDidChangeAnimated: " + String(localRegion))
//    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

